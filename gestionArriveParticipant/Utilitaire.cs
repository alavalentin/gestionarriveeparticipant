﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using QRCoder;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace gestionArriveParticipant
{
    public class Utilitaire
    {


        /// <summary>
        /// Méthode permettant de générer une clé WPA - 20 caractère -- 160 bits
        /// </summary>
        /// <returns></returns>
        public static String genererCle()
        {
            Collection<char> characters = new Collection<char>() { ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '\'', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~' };
            String asciikey = "";
            String hexkey = "";
            asciikey = "";
            hexkey = "";
            Random nbRand = new Random();
            short longeur = 20;
            for (int i = 0; i < longeur; i++)
            {
                asciikey += characters[(nbRand.Next(0, characters.Count))]; //rand. ascii
                hexkey += String.Format("{0:X}", Convert.ToInt32(asciikey[i])); // hex equiv.
            }
            return asciikey;
        }
        /// <summary>
        /// Méthode permettant de générer un QR Code
        /// </summary>
        /// <param name="text">Texte qui sera dans le QR Code</param>
        /// <param name="controlImage">Control qui contiendra l'image du QR Code</param>
        public static void genererQrCode(String text, Image controlImage)
        {
            //Génération du QR Code
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);

            //Affichage du QR Code
            controlImage.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(qrCode.GetGraphic(20).GetHbitmap(),
                                                                                           IntPtr.Zero,
                                                                                           System.Windows.Int32Rect.Empty,
                                                                                           BitmapSizeOptions.FromWidthAndHeight(400, 400));
        }

    }
}
