﻿using gestionArriveParticipant.Model;
using MahApps.Metro.Controls;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace gestionArriveParticipant
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {

        private OracleDatabase UneConnexion;

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Méthode permettant de rafraichir la liste cbParticipant
        /// </summary>
        private void refreshListe()
        {
            cbParticipant.ItemsSource = null;
            var query =
              from participant in UneConnexion.PARTICIPANT
              where participant.DATEENREGISTREMENTARRIVEE == null
              select participant;
            cbParticipant.ItemsSource = query.ToList();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            UneConnexion = ((WindowLogin)Owner).oracleContext;
            this.refreshListe();
            cbParticipant.SelectedValuePath = "ID";
            cbParticipant.DisplayMemberPath = "INFOPARTICIPANT";
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnArrivee_Click(object sender, RoutedEventArgs e)
        {
            if (cbParticipant.SelectedValue != null && dateArrivee.Value != null)
            {
                string clee = Utilitaire.genererCle();
                ((PARTICIPANT)cbParticipant.SelectedItem).CLEWIFI = clee;
                ((PARTICIPANT)cbParticipant.SelectedItem).DATEENREGISTREMENTARRIVEE = dateArrivee.Value;
                UneConnexion.SaveChanges();

                Utilitaire.genererQrCode(cbParticipant.SelectedValue.ToString(), imageQr);

                this.refreshListe();
                this.dateArrivee.Value = null;
                MessageBox.Show("La date d'arrivée et la clé wifi pour ce participant ont bien été enregistré");
            }
            else
            {
                MessageBox.Show("Un des champs n'a pas été rempli");
            }

        }
    }
}
